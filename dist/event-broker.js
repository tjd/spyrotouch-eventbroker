(function() {
  var Backbone, Chaplin, EventBroker, Q, mediator, _,
    __slice = [].slice;

  if (typeof window !== "undefined" && window !== null) {
    Backbone = window.Backbone;
    Chaplin = window.Chaplin;
    _ = window._;
    Q = window.Q;
  } else {
    Backbone = require('backbone');
    Chaplin = require('chaplin');
    _ = require('lodash');
    Q = require('q');
  }

  if (typeof global !== "undefined" && global !== null) {
    if (global.mediator != null) {
      mediator = global.mediator;
    } else {
      mediator = _.clone(Chaplin.mediator);
      global.mediator = mediator;
    }
  } else {
    mediator = _.clone(Chaplin.mediator);
  }

  mediator.namespaceChannels = {};

  EventBroker = {
    trigger: function() {
      var args;
      args = 1 <= arguments.length ? __slice.call(arguments, 0) : [];
      if (this._namespace != null) {
        args.push(this._namespace);
      }
      return Backbone.Events.trigger.apply(this, args);
    },
    registerNameSpaceChannel: function(namespace) {
      return mediator.namespaceChannels[namespace] = _.clone(Chaplin.mediator);
    },
    subscribeRequest: function(type, handler) {
      if (this._namespace != null) {
        mediator.namespaceChannels[this._namespace].removeHandlers([type]);
        return mediator.namespaceChannels[this._namespace].setHandler(type, handler, this);
      } else {
        mediator.removeHandlers([type]);
        return mediator.setHandler(type, handler, this);
      }
    },
    unsubscribeRequest: function(type) {
      if (this._namespace != null) {
        return mediator.namespaceChannels[this._namespace].removeHandlers([type]);
      } else {
        return mediator.removeHandlers([type]);
      }
    },
    unsubscribeAllRequests: function() {
      if (this._namespace != null) {
        mediator.namespaceChannels[this._namespace].removeHandlers(this);
      }
      return mediator.removeHandlers(this);
    },
    publishRequest: function() {
      var args, result, type, _ref;
      type = arguments[0], args = 2 <= arguments.length ? __slice.call(arguments, 1) : [];
      if (typeof type !== 'string') {
        throw new TypeError('EventBroker#publishRequest: ' + 'type argument must be a string');
      }
      result = Q.defer();
      if (this._namespace != null) {
        if ((args[0] != null) && typeof args[0] === 'object') {
          args[0]['_namespace'] = this._namespace;
          args[0]['nameSpace'] = this._namespace;
        }
        (_ref = mediator.namespaceChannels[this._namespace]).execute.apply(_ref, [type, result].concat(__slice.call(args)));
        args.push(this._namespace);
      }
      mediator.execute.apply(mediator, [type, result].concat(__slice.call(args)));
      return result.promise;
    },
    subscribeEvent: function(type, handler) {
      if (typeof type !== 'string') {
        throw new TypeError('EventBroker#subscribeEvent: ' + 'type argument must be a string');
      }
      if (typeof handler !== 'function') {
        throw new TypeError('EventBroker#subscribeEvent: ' + 'handler argument must be a function');
      }
      if (this._namespace != null) {
        mediator.namespaceChannels[this._namespace].unsubscribe(type, handler, this);
        return mediator.namespaceChannels[this._namespace].subscribe(type, handler, this);
      } else {
        mediator.unsubscribe(type, handler, this);
        return mediator.subscribe(type, handler, this);
      }
    },
    subscribeEventOnce: function(type, handler) {
      if (typeof type !== 'string') {
        throw new TypeError('EventBroker#subscribeEventOnce: ' + 'type argument must be a string');
      }
      if (typeof handler !== 'function') {
        throw new TypeError('EventBroker#subscribeEventOnce: ' + 'handler argument must be a function');
      }
      if (this._namespace != null) {
        mediator.namespaceChannels[this._namespace].unsubscribe(type, handler, this);
        return mediator.namespaceChannels[this._namespace].subscribeOnce(type, handler, this);
      } else {
        mediator.unsubscribe(type, handler, this);
        return mediator.subscribeOnce(type, handler, this);
      }
    },
    unsubscribeEvent: function(type, handler) {
      if (typeof type !== 'string') {
        throw new TypeError('EventBroker#unsubscribeEvent: ' + 'type argument must be a string');
      }
      if (typeof handler !== 'function') {
        throw new TypeError('EventBroker#unsubscribeEvent: ' + 'handler argument must be a function');
      }
      if (this._namespace != null) {
        return mediator.namespaceChannels[this._namespace].unsubscribe(type, handler);
      } else {
        return mediator.unsubscribe(type, handler);
      }
    },
    unsubscribeAllEvents: function() {
      if (this._namespace != null) {
        return mediator.namespaceChannels[this._namespace].unsubscribe(null, null, this);
      } else {
        return mediator.unsubscribe(null, null, this);
      }
    },
    publishEvent: function() {
      var args, type, _ref;
      type = arguments[0], args = 2 <= arguments.length ? __slice.call(arguments, 1) : [];
      if (typeof type !== 'string') {
        throw new TypeError('EventBroker#publishEvent: ' + 'type argument must be a string');
      }
      if (this._namespace != null) {
        (_ref = mediator.namespaceChannels[this._namespace]).publish.apply(_ref, [type].concat(__slice.call(args)));
        args.push(this._namespace);
      }
      return mediator.publish.apply(mediator, [type].concat(__slice.call(args)));
    }
  };

  if (typeof Object.freeze === "function") {
    Object.freeze(EventBroker);
  }

  if (typeof window !== "undefined" && window !== null) {
    window.SpyroEventBroker = EventBroker;
  } else {
    module.exports = EventBroker;
  }

}).call(this);
