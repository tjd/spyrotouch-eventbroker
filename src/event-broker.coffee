if window?
  Backbone = window.Backbone
  Chaplin = window.Chaplin
  _ = window._
  Q = window.Q
else
  Backbone = require 'backbone'
  Chaplin = require 'chaplin'
  _ = require 'lodash'
  Q = require 'q'

# compartir mediator entre modulos nodejs. solo para node
if global?
  if global.mediator?
    mediator = global.mediator
  else
    mediator = _.clone(Chaplin.mediator)
    global.mediator = mediator

else
  mediator = _.clone(Chaplin.mediator)

mediator.namespaceChannels = {}

# # Modificar 'trigger' de backbone para que ultimo argumento sea _namespace
# Backbone.Events.trigger = (name) ->
#   return this  unless this._events
#   args = slice.call(arguments, 1)
#   console.log('----------------trigger ' + name);
#   if this._namespace?
#     args.push this._namespace
#   return this  unless eventsApi(this, "trigger", name, args)
#   events = this._events[name]
#   allEvents = this._events.all
#   triggerEvents events, args  if events
#   triggerEvents allEvents, arguments  if allEvents
#   this
# #####


EventBroker =

  # Sobrecargar 'trigger' de backbone para que ultimo argumento sea _namespace
  trigger: (args...) ->
    if this._namespace?
      args.push this._namespace
    Backbone.Events.trigger.apply(this,args)

  registerNameSpaceChannel: (namespace)->
    # Los canales son mediators pero solo para un namespace
    mediator.namespaceChannels[namespace] = _.clone(Chaplin.mediator)

  # Añadir funcionalidad para request sobre Chaplin EventBroker
  ####
  subscribeRequest: (type, handler) ->
    
    if this._namespace?
      mediator.namespaceChannels[this._namespace].removeHandlers [type]
      mediator.namespaceChannels[this._namespace].setHandler type, handler, this
    else
      # Ensure that a handler isn’t registered twice.
      mediator.removeHandlers [type]

      mediator.setHandler type, handler, this

  unsubscribeRequest: (type) ->
    if this._namespace?
      mediator.namespaceChannels[this._namespace].removeHandlers [type]
    else
      # Remove global handler.
      mediator.removeHandlers [type]

  # Unbind all global handlers.
  unsubscribeAllRequests: ->

    if this._namespace?
      mediator.namespaceChannels[this._namespace].removeHandlers this

    # Remove all handlers with a context of this subscriber.
    mediator.removeHandlers this

  publishRequest: (type, args...) ->
    if typeof type isnt 'string'
      throw new TypeError 'EventBroker#publishRequest: ' +
        'type argument must be a string'

    result = Q.defer()
    
    if this._namespace?
      
      # en la practica se ha estandarizado que en vez de multiples parametros
      # solo se transmite un objeto con todos los parametros
      if args[0]? and typeof args[0] is 'object'
        args[0]['_namespace'] = this._namespace
        args[0]['nameSpace'] = this._namespace
      
      mediator.namespaceChannels[this._namespace].execute type, result, args...
      # añadir como ultimo param el namespace para poder gestionar el evento
      # de forma global a todos los canales y saber el origen: para aquellas
      # clases demultiplexadas
      
      args.push this._namespace

    # Publish global handler.
    mediator.execute type, result, args...

    # Devolver promesa, el handler se encargara de dar valor cuando la resuelva
    result.promise
  ####

  # funciones sobrescritas de chaplin
  subscribeEvent: (type, handler) ->
    if typeof type isnt 'string'
      throw new TypeError 'EventBroker#subscribeEvent: ' +
        'type argument must be a string'
    if typeof handler isnt 'function'
      throw new TypeError 'EventBroker#subscribeEvent: ' +
        'handler argument must be a function'

    if this._namespace?
      mediator.namespaceChannels[this._namespace].unsubscribe type, handler, this
      mediator.namespaceChannels[this._namespace].subscribe type, handler, this
    else
      # Ensure that a handler isn’t registered twice.
      mediator.unsubscribe type, handler, this

      # Register global handler, force context to the subscriber.
      mediator.subscribe type, handler, this


  subscribeEventOnce: (type, handler) ->
    if typeof type isnt 'string'
      throw new TypeError 'EventBroker#subscribeEventOnce: ' +
        'type argument must be a string'
    if typeof handler isnt 'function'
      throw new TypeError 'EventBroker#subscribeEventOnce: ' +
        'handler argument must be a function'

    if this._namespace?
      mediator.namespaceChannels[this._namespace].unsubscribe type, handler, this
      mediator.namespaceChannels[this._namespace].subscribeOnce type, handler, this
    else
      # Ensure that a handler isn’t registered twice.
      mediator.unsubscribe type, handler, this

      # Register global handler, force context to the subscriber.
      mediator.subscribeOnce type, handler, this

  unsubscribeEvent: (type, handler) ->
    if typeof type isnt 'string'
      throw new TypeError 'EventBroker#unsubscribeEvent: ' +
        'type argument must be a string'
    if typeof handler isnt 'function'
      throw new TypeError 'EventBroker#unsubscribeEvent: ' +
        'handler argument must be a function'

    if this._namespace?
      mediator.namespaceChannels[this._namespace].unsubscribe type, handler
    else
      # Remove global handler.
      mediator.unsubscribe type, handler

  # Unbind all global handlers.
  unsubscribeAllEvents: ->

    if this._namespace?
      mediator.namespaceChannels[this._namespace].unsubscribe null, null, this
    else
      # Remove all handlers with a context of this subscriber.
      mediator.unsubscribe null, null, this

  publishEvent: (type, args...) ->
    if typeof type isnt 'string'
      throw new TypeError 'EventBroker#publishEvent: ' +
        'type argument must be a string'

    if this._namespace?
      mediator.namespaceChannels[this._namespace].publish type, args...
      # añadir como ultimo param el namespace para poder gestionar el evento
      # de forma global a todos los canales y saber el origen: para aquellas
      # clases demultiplexadas
      args.push this._namespace

    # Publish global handler.
    mediator.publish type, args...


# You’re frozen when your heart’s not open.
Object.freeze? EventBroker

# poder requerirlo en node y browser
if window?
  window.SpyroEventBroker = EventBroker
else
  # Return our creation.
  module.exports = EventBroker



